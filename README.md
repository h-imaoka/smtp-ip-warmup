# smtp-ip-warmup

# python
2.7

# One shot
`./send.py < send_list`

# Via cron
`*/20 * * * * root /home/ec2-user/send.py < /home/ec2-user/send-list`
