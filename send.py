#!/bin/env python
# -*- coding: utf-8 -*-
import smtplib
from email.mime.text import MIMEText
from email.utils import formatdate
import urllib2
import json
import sys


E_FROM_ADDRESS = 'noreply@pjlv.jp'
FROM_ADDRESS = 'noreply@pjlv.jp'

TO_ADDRESS = 'imaoka@cloudpack.jp'


def create_message(from_addr, to_addr, subject, body):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Date'] = formatdate()
    return msg


def send(from_addr, to_addrs, msg):
    smtpobj = smtplib.SMTP('172.16.0.217')
    smtpobj.ehlo()
    smtpobj.sendmail(from_addr, to_addrs, msg.as_string())
    smtpobj.close()


if __name__ == '__main__':

    response = urllib2.urlopen('https://newsapi.org/v2/top-headlines?country=us&apiKey=ed9064420db14eeaa4f6f53d40f7db17')
    js = response.read().decode('utf-8')
    j = json.loads(js)

    subject =  j['articles'][0]['source']['name']
    body = "{0}\n{1}".format(
        j['articles'][0]['title'],
        j['articles'][0]['description']
    )
    to_addr = TO_ADDRESS

    msg = create_message(FROM_ADDRESS, to_addr, subject, body)
    # to address loop
    for to_addr in sys.stdin:
        send(E_FROM_ADDRESS, to_addr, msg)
